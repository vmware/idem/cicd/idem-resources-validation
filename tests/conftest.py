import asyncio
import pathlib
import sys
import unittest.mock as mock

import pop.hub
import pytest


@pytest.fixture(scope="session")
def code_dir() -> pathlib.Path:
    return pathlib.Path(__file__).parent.parent.absolute()


@pytest.fixture(scope="module")
def event_loop():
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="module", name="hub")
def tpath_hub(code_dir, event_loop):
    """
    Add "idem_plugin" to the test path
    """
    TPATH_DIR = str(code_dir / "tests" / "tpath")

    with mock.patch("sys.path", [TPATH_DIR] + sys.path):
        hub = pop.hub.Hub()
        hub.pop.loop.CURRENT_LOOP = event_loop
        hub.pop.sub.add(dyne_name="idem")
        hub.pop.config.load(hub.idem.CONFIG_LOAD, "idem", parse_cli=False)
        hub.idem.RUNS = {"test": {}}

        yield hub


@pytest.fixture(scope="session", name="hub")
def schema_hub(hub):
    hub.pop.sub.add(dyne_name="idem")
    yield hub
